package com.nelson.rodriguez.entities;

import java.sql.Date;

public class Libro {
	Integer id;
	String titulo;
	Date fechapublicacion;
	String autor;

	public int getId() {
		return id;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Date getFechapublicacion() {
		return fechapublicacion;
	}

	public void setFechapublicacion(Date fechapublicacion) {
		this.fechapublicacion = fechapublicacion;
	}

	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", fechapublicacion=" + fechapublicacion + ", autor=" + autor
				+ "]";
	}

	

}
