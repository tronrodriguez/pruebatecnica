package com.nelson.rodriguez;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.nelson.rodriguez.repository.ILibreriaRepo;
import com.nelson.rodriguez.service.ILibreriaService;
import com.nelson.rodriguez.service.LibreriaServiceImpl;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
 class PruebaadyaApplication implements CommandLineRunner {
	

	
	private static Logger LOG=LoggerFactory.getLogger(PruebaadyaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PruebaadyaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//LOG.info(service.obtenerListaLibros().toString());
		
	}

}
