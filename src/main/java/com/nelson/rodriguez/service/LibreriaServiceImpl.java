package com.nelson.rodriguez.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import com.nelson.rodriguez.entities.Libro;
import com.nelson.rodriguez.repository.ILibreriaRepo;


@Service
public class LibreriaServiceImpl implements ILibreriaService {
	

	private static Logger LOG=LoggerFactory.getLogger(LibreriaServiceImpl.class);

	@Override
	public List<Libro> obtenerListaLibros() {
		List<Libro> listado = new ArrayList<Libro>();
		Libro libro=new Libro();
		libro.setAutor("tron");
		
		listado.add(libro);
		return listado;
	}

}
