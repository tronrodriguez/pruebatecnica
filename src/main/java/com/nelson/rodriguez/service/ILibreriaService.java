package com.nelson.rodriguez.service;

import java.util.List;

import com.nelson.rodriguez.entities.Libro;

public interface ILibreriaService {
	
	
	List<Libro> obtenerListaLibros();

}
