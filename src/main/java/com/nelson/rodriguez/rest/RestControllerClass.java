package com.nelson.rodriguez.rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.nelson.rodriguez.entities.Libro;
import com.nelson.rodriguez.repository.ILibreriaRepo;
import com.nelson.rodriguez.repository.LibreriaRepoImpl;
import com.nelson.rodriguez.utils.Utils;


@RestController
@RequestMapping("libreria")
public class RestControllerClass {
	private JdbcTemplate jdbcTemplate;
	private static Logger LOG = LoggerFactory.getLogger(RestControllerClass.class);
	
	@Autowired
	private LibreriaRepoImpl repo;
	
	@GetMapping
	public String listarLibros() {
		jdbcTemplate = new JdbcTemplate(Utils.conectarDB());
		LOG.info("Listar libros");
		return repo.obtenerListaLibros().toString();
	}
	
	@PostMapping
	public String insertarLibro(@RequestBody Libro libro) {
		int idGenerado=repo.insertarLibro(libro);
        LOG.info("idLibro creado: " + idGenerado);
        JSONObject jsonRta=new JSONObject();
        jsonRta.put("id", idGenerado);
		return jsonRta.toString();
	}
	
	@DeleteMapping(value = "/{id}")
	public String borrarLibro(@PathVariable("id") Integer id) {
		LOG.info("Borrar libro id: "+id);
		repo.borrarLibro(id);
		JSONObject jsonRta=new JSONObject();
		jsonRta.put("status", "id: "+id+" "+"borrado");
		return jsonRta.toString();
	}
	
	@PutMapping
	public String actualizarLibro(@RequestBody Libro libro) {
		LOG.info("Actualizar libro: "+libro.toString());
		String status=repo.actalizarLibro(libro);
		JSONObject jsonRta=new JSONObject();
		jsonRta.put("status", status);
		return jsonRta.toString();
	}

}
