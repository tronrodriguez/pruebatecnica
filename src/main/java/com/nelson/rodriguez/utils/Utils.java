package com.nelson.rodriguez.utils;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Utils {
	private static String host="localhost";
    private static String port="3306";
    private static String dbName="pruebaadya";
    private static String username="root";
    private static String password="irf840";

	public static void loguer(String m) {
		System.out.println(m);
	}
	
	public static final DriverManagerDataSource conectarDB() {
		loguer("Connecting database....");
		DriverManagerDataSource dataSource=new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");  
        dataSource.setUrl("jdbc:mysql://" + host + ":" + port + "/"+ dbName + "?autoReconnect=true");         
        dataSource.setUsername(username);  
        dataSource.setPassword(password);   
        return dataSource;
	}

}
