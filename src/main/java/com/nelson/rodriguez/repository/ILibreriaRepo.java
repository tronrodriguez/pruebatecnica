package com.nelson.rodriguez.repository;

import java.util.List;

import org.json.JSONArray;

import com.nelson.rodriguez.entities.Libro;

public interface ILibreriaRepo {
	
	JSONArray obtenerListaLibros();
	
	
	int insertarLibro(Libro libro);
	
	
	void borrarLibro(int id);
	
	String actalizarLibro(Libro libro);

}
