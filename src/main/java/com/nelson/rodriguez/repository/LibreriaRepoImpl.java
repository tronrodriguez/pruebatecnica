package com.nelson.rodriguez.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.nelson.rodriguez.entities.Libro;
import com.nelson.rodriguez.utils.Utils;

@Service
public class LibreriaRepoImpl implements ILibreriaRepo {

	private static Logger LOG = LoggerFactory.getLogger(LibreriaRepoImpl.class);
	private JdbcTemplate jdbcTemplate;

	@Override
	public JSONArray obtenerListaLibros() {
		Utils.loguer("Repo obtenerListaLibros");
		jdbcTemplate = new JdbcTemplate(Utils.conectarDB());
		String sql = "select * from libros order by id asc";
		List<Libro> listaLibros = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Libro.class));
		JSONArray jsonArray = new JSONArray();
		for (Libro libro : listaLibros) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", libro.getId());
			jsonObject.put("titulo", libro.getTitulo());
			jsonObject.put("fechaPublicacion", libro.getFechapublicacion());
			jsonObject.put("autor", libro.getAutor());
			jsonArray.put(jsonObject);
			Utils.loguer(libro.toString());
		}
		return jsonArray;

	}

	@Override
	public int insertarLibro(Libro libro) {
		jdbcTemplate = new JdbcTemplate(Utils.conectarDB());
		LOG.info("Insertar libro: " + libro.toString());
		String sql = "insert into libros (titulo,fechaPublicacion,autor) values(?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(sql, libro.getTitulo(), libro.getFechapublicacion(), libro.getAutor());
		String status = "error";
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) {
				PreparedStatement statement = null;
				try {
					statement = con.prepareStatement(sql, new String[] { "id" });
					statement.setString(1, libro.getTitulo());
					statement.setDate(2, libro.getFechapublicacion());
					statement.setString(3, libro.getAutor());
				} catch (SQLException ex) {
					Utils.loguer(ex.toString());
				}
				return statement;
			}
		}, keyHolder);
		int idGenerado = keyHolder.getKey().intValue();
		return idGenerado;
	}

	@Override
	public void borrarLibro(int id) {
		jdbcTemplate = new JdbcTemplate(Utils.conectarDB());
		String sql="delete from libros where id=?";
		jdbcTemplate.update(sql,id);
		
	}

	@Override
	public String actalizarLibro(Libro libro) {
		jdbcTemplate = new JdbcTemplate(Utils.conectarDB());
		String sql="select * from libros where id="+libro.getId();
		Utils.loguer(sql);
		String status="error";
		List<Libro> listaLibros=jdbcTemplate.query(sql, new BeanPropertyRowMapper(Libro.class));
		
		if (listaLibros.size()>0) {
			Libro libroAntes=listaLibros.get(0);
			Utils.loguer("Libro a actualizar: "+libroAntes.toString());
			if (libro.getAutor()==null) {
				libro.setAutor(libroAntes.getAutor());
			}
			if (libro.getTitulo()==null) {
				libro.setTitulo(libroAntes.getTitulo());
			}
			if (libro.getFechapublicacion()==null) {
				libro.setFechapublicacion(libroAntes.getFechapublicacion());
			}
		}
		sql="update libros set titulo=?, fechaPublicacion=?,autor=? where id=?";
		
		if (jdbcTemplate.update(sql,libro.getTitulo(),libro.getFechapublicacion(),libro.getAutor(),libro.getId())==1){
			status="actualizado";
		}
		return status;
	}

}
