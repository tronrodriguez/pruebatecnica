package com.nelson.rodriguez;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.nelson.rodriguez.rest.RestControllerClass;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RestControllerClass.class)
@WebAppConfiguration
class PruebaadyaApplicationTests {
	protected MockMvc mvc;
	@Autowired
	WebApplicationContext webApplicationContext;

	@Test
	void contextLoads() {
	}

}
