package com.nelson.rodriguez;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nelson.rodriguez.entities.Libro;
import com.nelson.rodriguez.repository.ILibreriaRepo;
import com.nelson.rodriguez.repository.LibreriaRepoImpl;
import com.nelson.rodriguez.rest.RestControllerClass;



@RunWith(SpringRunner.class)
@WebMvcTest(value = RestControllerClass.class)
@WebAppConfiguration
public class LibreriaControllerTest  {

	private static Logger LOG=LoggerFactory.getLogger(LibreriaControllerTest.class);
	
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LibreriaRepoImpl repo;
	
	@Autowired
	ObjectMapper mapper;
	
	@Test
	   public void getProductsList() throws Exception {
		LOG.info("Test getProductsList");
		JSONArray arrayLibros = new JSONArray();
		Mockito.when(repo.obtenerListaLibros()).thenReturn(arrayLibros);
		LOG.info("Size: "+arrayLibros.length());
		mockMvc.perform(get("/libreria")).andExpect(status().isOk());
		
		
	   }

}
